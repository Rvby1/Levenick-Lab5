package virtualDisk;

/**
 * Inode.java
 *
 * @author levenick
 */
public class Inode extends Block {

    public Inode() {
        super();
    }

    public short getSize() {
        return decodeLink(0);
    }

    public short getDirectLink() {
        return decodeLink(2);
    }

    public short getIndirectLink() {
        return decodeLink(4);
    }

    public short getDoubleIndirectLink() {
        return decodeLink(6);
    }

    public void setSize(short size) {
        setLink(0, size);
    }

    public void setDirectLink(short directLink) {
        setLink(2, directLink);
    }

    public void setIndirectLink(short indirectLink) {
        setLink(4, indirectLink);
    }

    public void setDoubleIndirectLink(short doubleIndirectLink) {
        setLink(6, doubleIndirectLink);
    }

    public String toString() {
        String returnMe = "Inode: ";
        returnMe += "\tsize=" + getSize();
        returnMe += "\tdirectLink=" + getDirectLink();
        short indirectBlock = getIndirectLink();
        if (indirectBlock == 0) {
            returnMe += "\tno indirects!";
        } else {
            returnMe += "\tindirectLink=" + this.getIndirectLink();
        }

        short doubleIndirectBlock = this.getDoubleIndirectLink();
        if (doubleIndirectBlock == 0) {
            returnMe += "\tno double indirects!";
        } else {
            returnMe += "\tdoubleIndirectLink =" + this.getDoubleIndirectLink();
        }
        return returnMe;
    } // toString()

    //the function that is called to start the chain of loading a file  
    String load() {
        // always direct... 
        //check to see if the indirect and double indirect fields are empty. If they are, do nothing. If they aren't, read the data and add it to returnMe
        String returnMe = "";

        returnMe += makeString(Globals.getTheDisk().read(getDirectLink()));  // read the direct link

        //if there's an indirect link...
        //if there's a double indirect link...
        return returnMe;
    }

    private String makeString(byte[] data) {
        String returnMe = "";

        for (int i = 0; i < data.length; i++) {
            returnMe += (char) data[i];
        }

        return returnMe;
    }
}  // Inode
