package virtualDisk;

import java.util.ArrayList;

/**
 *
 * @author djk19 Mar 7, 2018
 */
public class FileList extends ArrayList<File>{
    public boolean fileInList(String fileName) { 
        for(File file : this) { 
            if(file.getName().equals(fileName)) { 
                return true;
            } 
        }
        return false;
    }
    
    public File getFile(String fileName) { 
        for(File file : this) { 
            if(file.getName().equals(fileName)) { 
                return file;
            }
        }
        return new File("FILEDOESNOTEXIST", new Inode());
    }

    public String listFiles() {
        String returnMe = "File List: \n";
        for(File file : this) { 
            returnMe += "File Name = " + file.getName() + "; iNode = " + file.getiNode().toString() + "\n";
        }
        return returnMe;
    }
}
