package virtualDisk;

/**
 *
 * @author djk19 Mar 7, 2018
 */
public class File {

    private String name;
    private Inode iNode;

    public File(String name, Inode iNode) {
        this.name = name;
        this.iNode = iNode;
    }

    /**
     * @return the iNode
     */
    public Inode getiNode() {
        return iNode;
    }

    /**
     * @param iNode the iNode to set
     */
    public void setiNode(Inode iNode) {
        this.iNode = iNode;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public String toString() { 
        String returnMe = "";
        returnMe += "FileName = " + this.getName();
        returnMe += "\nInode = " + this.iNode.toString();
        return returnMe;
    }
}
