package virtualDisk;

/**
 *
 * @author djk19 Mar 6, 2018
 */
public class FileSystem {

    Disk disk;
    FileList fileList;

    public FileSystem() {
        Globals.init();
        this.disk = Globals.getTheDisk();
        this.fileList = new FileList();
    }

    //TODO: Add check to see if file already exists. If it does, clear the iNode for that file, then refill that iNode with the new information. 
    public void createFile(String fileName, String sToWrite) {      
        int remainingBlocks = this.disk.getFreeDataBlockListSize();
        if(sToWrite.length() / Globals.getBlockLength() + 6 > remainingBlocks) { //works, but not 100% accurate. Might be worth tuning. 
            Globals.complain("OUT OF SPACE!");
            return;
        }
        if(this.disk.getFreeINodeListSize() == 0) { 
            Globals.complain("OUT OF INODES!");
            return;
        }
        
        Inode iNode = this.disk.getINode();
        iNode.setSize((short) sToWrite.length());

        if (sToWrite.length() <= Globals.getBlockLength()) {
            this.writeToDirectDataBlock(iNode, sToWrite);
        } else if (sToWrite.length() > Globals.getBlockLength()
                && sToWrite.length() <= Globals.getBlockLength() + Globals.getBlockLength() * (Globals.getLinksPerBlock())) {

            this.writeToDirectDataBlock(iNode, sToWrite.substring(0, Globals.getBlockLength()));
            String remainingSToWrite = sToWrite.substring(Globals.getBlockLength());
            this.writeToIndirectDataBlock(iNode, remainingSToWrite);
        } else if (sToWrite.length() > Globals.getBlockLength() + Globals.getBlockLength() * (Globals.getLinksPerBlock())
                && sToWrite.length() <= Globals.getBlockLength() + Globals.getBlockLength()
                * (Globals.getLinksPerBlock()) + Globals.getBlockLength() * (Globals.getLinksPerBlock()) * (Globals.getLinksPerBlock())) {

            this.writeToDirectDataBlock(iNode, sToWrite.substring(0, Globals.getBlockLength()));
            String remainingSToWrite = sToWrite.substring(Globals.getBlockLength());
            this.writeToIndirectDataBlock(iNode, remainingSToWrite);
            remainingSToWrite = sToWrite.substring(Globals.getBlockLength() * (Globals.getLinksPerBlock()) + Globals.getBlockLength());
            this.writeToDoubleIndirectBlock(iNode, remainingSToWrite);
        } else {
            Globals.complain("STRING TOO LARGE!!! MAX LENGTH IS " + Globals.getBlockLength() * 6);
        }

        System.out.println(this.disk.toString());
        this.fileList.add(new File(fileName, iNode));
    }

    private void writeToDirectDataBlock(Inode iNode, String sToWrite) {
        short directBlockNum = this.disk.getDataBlockNum();
        iNode.setDirectLink(directBlockNum);
        this.disk.write(directBlockNum, sToWrite);
    }

    private void writeToIndirectDataBlock(Inode iNode, String sToWrite) {
        short indirectPointerBlockNum = this.disk.getDataBlockNum();
        iNode.setIndirectLink(indirectPointerBlockNum);
        DataBlock indirectPointerBlock = this.disk.getDataBlock(indirectPointerBlockNum);
        short indexOfIndirectlyLinkedBlocks = 0;
        boolean finishedWriting = false;
        for (short i = 0; i < Globals.getLinksPerBlock(); i++) {
            short sToWriteSubstringIndex = (short) (i * Globals.getBlockLength());
            short indirectlyLinkedBlockNum = this.disk.getDataBlockNum();
            if (sToWriteSubstringIndex + Globals.getBlockLength() >= sToWrite.length()) {
                this.disk.write(indirectlyLinkedBlockNum, sToWrite.substring(sToWriteSubstringIndex));
                finishedWriting = true;
            } else {
                this.disk.write(indirectlyLinkedBlockNum, sToWrite.substring(sToWriteSubstringIndex,
                        sToWriteSubstringIndex + Globals.getBlockLength()));
            }
            indirectPointerBlock.setLink(indexOfIndirectlyLinkedBlocks, indirectlyLinkedBlockNum);
            if (finishedWriting) {
                break;
            }
            indexOfIndirectlyLinkedBlocks += 2;
        }
    }

    //error in this code somewhere. Find it! 
    private void writeToDoubleIndirectBlock(Inode iNode, String sToWrite) {
        short doubleIndirectPointerBlockOneNum = this.disk.getDataBlockNum();
        iNode.setDoubleIndirectLink(doubleIndirectPointerBlockOneNum);
        DataBlock doubleIndirectPointerBlockOne =  this.disk.getDataBlock(doubleIndirectPointerBlockOneNum);
        short indexOfDoubleIndirectPointerBlockTwos = 0;
        boolean finishedWriting = false;
        for (short i = 0; i < Globals.getLinksPerBlock(); i++) {
            short doubleIndirectPointerBlockTwoNum = this.disk.getDataBlockNum();
            DataBlock doubleIndirectPointerBlockTwo = this.disk.getDataBlock(doubleIndirectPointerBlockTwoNum);
            short indexOfDoubleIndirectlyLinkedBlocks = 0;
            for (short j = 0; j < Globals.getLinksPerBlock(); j++) {
                short sToWriteSubstringIndex = (short) (i * (Globals.getLinksPerBlock()) * Globals.getBlockLength() + Globals.getBlockLength() * j);
                short doubleIndirectlyLinkedBlockNum = this.disk.getDataBlockNum();
                if (sToWriteSubstringIndex + Globals.getBlockLength() >= sToWrite.length()) {
                    this.disk.write(doubleIndirectlyLinkedBlockNum, sToWrite.substring(sToWriteSubstringIndex));
                    finishedWriting = true;
                } else {
                    this.disk.write(doubleIndirectlyLinkedBlockNum, sToWrite.substring(sToWriteSubstringIndex,
                            sToWriteSubstringIndex + Globals.getBlockLength()));
                }
                doubleIndirectPointerBlockTwo.setLink(indexOfDoubleIndirectlyLinkedBlocks, doubleIndirectlyLinkedBlockNum);
                if (finishedWriting) {
                    break;
                }
                indexOfDoubleIndirectlyLinkedBlocks += 2;
            }
            doubleIndirectPointerBlockOne.setLink(indexOfDoubleIndirectPointerBlockTwos, doubleIndirectPointerBlockTwoNum);
            if (finishedWriting) {
                break;
            }
            indexOfDoubleIndirectPointerBlockTwos += 2;
        }
    }

    private String readFile(File fileToRead) {
        String returnMe = "";
        Inode iNode = fileToRead.getiNode();
        if (iNode.getDirectLink() != 0) {
            returnMe += this.decodeByteArray(this.disk.getDataBlock(iNode.getDirectLink()).read());
        }
        if (iNode.getIndirectLink() != 0) {
            DataBlock indirectLinkPointerBlock = this.disk.getDataBlock(iNode.getIndirectLink());
            for (short i = 0; i < Globals.getBlockLength(); i += 2) {
                short indirectlyLinkedBlockNum = indirectLinkPointerBlock.decodeLink(i);
                if (indirectlyLinkedBlockNum == 0) {
                    break;
                }
                returnMe += this.decodeByteArray(this.disk.getDataBlock(indirectlyLinkedBlockNum).read());
            }
        }
        if (iNode.getDoubleIndirectLink() != 0) {
            DataBlock doubleIndirectPointerBlockOne = this.disk.getDataBlock(iNode.getDoubleIndirectLink());
            for (short i = 0; i < Globals.getBlockLength(); i += 2) {
                short doubleIndirectPointerBlockTwoNum = doubleIndirectPointerBlockOne.decodeLink(i);
                if (doubleIndirectPointerBlockTwoNum == 0) {
                    break;
                }
                for (short j = 0; j < Globals.getBlockLength(); j += 2) {
                    short doubleIndirectlyLinkedBlockNum = this.disk.getDataBlock(doubleIndirectPointerBlockTwoNum).decodeLink(j);
                    if (doubleIndirectlyLinkedBlockNum == 0) {
                        break;
                    }
                    returnMe += this.decodeByteArray(this.disk.getDataBlock(doubleIndirectlyLinkedBlockNum).read());
                }
            }
        }
        return returnMe;
    }

    public String loadFile(String fileName) {
        if (this.fileList.fileInList(fileName)) {
            File file = this.fileList.getFile(fileName);
            return this.readFile(file);
        } else {
            Globals.complain("ERROR! FILE NOT FOUND!");
            return "ERROR! FILE NOT FOUND!";
        }
    }

    public File getFile(String fileName) {
        if (this.fileList.fileInList(fileName)) {
            return this.fileList.getFile(fileName);
        } else {
            return new File("ERROR! FILE NOT FOUND!", new Inode());
        }
    }
    
    public void deleteFile(String fileName) {
        if(this.fileList.fileInList(fileName)) { 
            File fileToDelete = this.fileList.getFile(fileName);
            this.disk.clearINode(fileToDelete.getiNode());
            this.fileList.remove(fileToDelete);
        } else { 
            Globals.complain("FILE DOES NOT EXIST!");
        }
        System.out.println(this.disk.toString());
    }
    
    public void deleteAllFiles() { 
        for(File file : this.fileList) { 
            this.disk.clearINode(file.getiNode());
        }
        this.fileList.clear();
    }
    
    public String listFiles() {
        return this.fileList.listFiles();
    }
    
    private String decodeByteArray(byte[] bytes) {
        String returnMe = "";
        for (byte b : bytes) {
            returnMe += (char) b;
        }
        return returnMe;
    }
}
