package virtualDisk;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * Disk.java created by levenick on Oct 8, 2013 at 12:19:25 PM
 */
public class Disk {

    private BlockList freeINodeList;
    private BlockList freeDataBlockList;
    private Block[] blocks;

    public Disk() {
        blocks = new Block[Globals.getNumberOfBlocks()];
        this.freeDataBlockList = this.createFreeDataBlockList();
        this.freeINodeList = this.createFreeInodeList();
    }

    public BlockList createFreeInodeList() {
        BlockList inodeFreeList = new BlockList();
        for (short i = 0; i < Globals.getNumberOfInodes(); i++) {
            blocks[i] = new Inode();
            inodeFreeList.add(i);
        }
        return inodeFreeList;
    }

    public BlockList createFreeDataBlockList() {
        BlockList blockFreeList = new BlockList();
        for (short i = Globals.getNumberOfInodes(); i < Globals.getNumberOfBlocks(); i++) {
            blocks[i] = new DataBlock();
            blockFreeList.add(i);
        }
        return blockFreeList;
    }

    public void write(short blockNum, byte[] data) {
        if (data.length > Globals.getBlockLength()) {
            Globals.complain("Oops! That array is too big! data.length=" + data.length);
        }

        blocks[blockNum].write(data);
    }

    /**
     * Convert the String to an array of bytes and write them to the blockNumber
     *
     * @param blockNumber
     * @param s
     */
    public void write(Short blockNumber, String s) {
        byte[] data = new byte[Globals.getBlockLength()];
        for (int i = 0; i < s.length(); i++) {
            data[i] = (byte) s.charAt(i);
        }
        write(blockNumber, data);
    }

    public byte[] read(short blockNum) {
        if (blockNum >= Globals.getNumberOfBlocks()) {
            Globals.complain("Ack! Too big!! blockNum=" + blockNum);
        }

        return blocks[blockNum].read();
    }

    public String toString() {
        String returnMe = "I am a Disk";

        for (int i = 0; i < Globals.getNumberOfBlocks(); i++) {
            returnMe += "\n\t#" + i + ": " + blocks[i].toString();
        }

        return returnMe;
    }

    public short getDataBlockNum() {
        return this.freeDataBlockList.remove(0);
    }

    public DataBlock getDataBlock(short blockNum) {
        return (DataBlock) this.blocks[blockNum];
    }

    public Inode getINode() {
        return (Inode) this.blocks[this.freeINodeList.remove(0)];
    }

    public int getFreeINodeListSize() {
        return this.freeINodeList.size();
    }

    public int getFreeDataBlockListSize() {
        return this.freeDataBlockList.size();
    }

    public void clearINode(Inode iNode) {
        if (iNode.getDirectLink() != 0) {
            this.clearDataBlock((DataBlock) this.getDataBlock(iNode.getDirectLink()));
        }
        if (iNode.getIndirectLink() != 0) {
            DataBlock indirectLinkPointerBlock = this.getDataBlock(iNode.getIndirectLink());
            for (short i = 0; i < Globals.getBlockLength(); i += 2) {
                short indirectlyLinkedBlockNum = indirectLinkPointerBlock.decodeLink(i);
                if (indirectlyLinkedBlockNum == 0) {
                    break;
                }
                this.clearDataBlock(this.getDataBlock(indirectlyLinkedBlockNum));
            }
            this.clearDataBlock(indirectLinkPointerBlock);
        }
        if (iNode.getDoubleIndirectLink() != 0) {
            DataBlock doubleIndirectPointerBlockOne = this.getDataBlock(iNode.getDoubleIndirectLink());
            for (short i = 0; i < Globals.getBlockLength(); i += 2) {
                short doubleIndirectPointerBlockTwoNum = doubleIndirectPointerBlockOne.decodeLink(i);
                if (doubleIndirectPointerBlockTwoNum == 0) {
                    break;
                }
                for (short j = 0; j < Globals.getBlockLength(); j += 2) {
                    short doubleIndirectlyLinkedBlockNum = this.getDataBlock(doubleIndirectPointerBlockTwoNum).decodeLink(j);
                    if (doubleIndirectlyLinkedBlockNum == 0) {
                        break;
                    }
                    this.clearDataBlock(this.getDataBlock(doubleIndirectlyLinkedBlockNum));
                }
                this.clearDataBlock(this.getDataBlock(doubleIndirectPointerBlockTwoNum));
            }
            this.clearDataBlock(doubleIndirectPointerBlockOne);
        }
        iNode.clearData();
        this.addINode(iNode);
    }

    private void addINode(Inode iNode) {
        if (this.freeINodeList.contains(iNode)) {
            return;
        }
        for (short i = 0; i < Globals.getNumberOfInodes(); i++) {
            if (this.blocks[i].equals(iNode)) {
                this.freeINodeList.add(i);
            }
        }
    }

    private void clearDataBlock(DataBlock dataBlock) {
        dataBlock.clearData();
        this.addDataBlock(dataBlock);
        System.out.println(this.freeDataBlockList.toString());
    }

    private void addDataBlock(DataBlock dataBlock) {
        if (this.freeDataBlockList.contains(dataBlock)) {
            return;
        }
        for (short i = (short) (Globals.getNumberOfInodes() - 1); i < Globals.getNumberOfBlocks(); i++) {
            if (this.blocks[i].equals(dataBlock)) {
                this.freeDataBlockList.add(i);
            }
        }
    }

    /**
     * An example of using a JOptionPane
     *
     * @return the filename
     */
    String getFN() {
        String s = (String) JOptionPane.showInputDialog(
                new JFrame(),
                "Enter filename",
                "File name, please!",
                JOptionPane.PLAIN_MESSAGE,
                null,
                null,
                "foo");

        //If a string was returned, say so.
        if ((s != null) && (s.length() > 0)) {
            return s;
        }
        return null;
    }
}
