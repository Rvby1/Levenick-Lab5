package virtualDisk;

import java.awt.*;

/**
 *
 * @author djk19 Mar 7, 2018 9:21:20 PM
 */
public class MainPanel extends javax.swing.JPanel {

    public MainPanel() {
        initComponents();
        //setLayout(new BorderLayout());
        setVisible(true);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
    }
    
    public String getTextInTextArea() { 
        return this.stringToWriteTextArea.getText();
    }
    
    public void setTextInTextArea(String text) { 
        this.stringToWriteTextArea.setText(text);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        stringToWriteTextArea = new javax.swing.JTextArea();

        stringToWriteTextArea.setColumns(20);
        stringToWriteTextArea.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        stringToWriteTextArea.setLineWrap(true);
        stringToWriteTextArea.setRows(5);
        stringToWriteTextArea.setText("Enter the text you wish to save here!");
        stringToWriteTextArea.setToolTipText("");
        stringToWriteTextArea.setWrapStyleWord(true);
        jScrollPane1.setViewportView(stringToWriteTextArea);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea stringToWriteTextArea;
    // End of variables declaration//GEN-END:variables

}
