package virtualDisk;

import java.awt.*;
import javax.swing.JOptionPane;

/**
 *
 * @author djk19 Mar 7, 2018
 */
public class MainFrame extends javax.swing.JFrame {

    FileSystem fileSystem;
    MainPanel mainPanel;

    public MainFrame() {
        initComponents();
        reset();
        setTitle("Virtual Disk");
        setLayout(new BorderLayout());
        this.setLocationRelativeTo(null);
        mainPanel = new MainPanel();
        add(mainPanel, BorderLayout.CENTER);
        setVisible(true);
    }

    public void reset() {
        fileSystem = new FileSystem();
        //fileSystem.createFile("Test", "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz");
        //System.out.println(fileSystem.loadFile("Test"));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar1 = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        saveMenuButton = new javax.swing.JMenuItem();
        loadMenuButton = new javax.swing.JMenuItem();
        deleteMenuButton = new javax.swing.JMenuItem();
        deleteAllMenuButton = new javax.swing.JMenuItem();
        listFilesMenuButton = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        fileMenu.setText("File");
        fileMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileMenuActionPerformed(evt);
            }
        });

        saveMenuButton.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        saveMenuButton.setText("Save");
        saveMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveMenuButtonActionPerformed(evt);
            }
        });
        fileMenu.add(saveMenuButton);

        loadMenuButton.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        loadMenuButton.setText("Load");
        loadMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadMenuButtonActionPerformed(evt);
            }
        });
        fileMenu.add(loadMenuButton);

        deleteMenuButton.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_MASK));
        deleteMenuButton.setText("Delete");
        deleteMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteMenuButtonActionPerformed(evt);
            }
        });
        fileMenu.add(deleteMenuButton);

        deleteAllMenuButton.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        deleteAllMenuButton.setText("Delete All");
        deleteAllMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteAllMenuButtonActionPerformed(evt);
            }
        });
        fileMenu.add(deleteAllMenuButton);

        listFilesMenuButton.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
        listFilesMenuButton.setText("List Files");
        listFilesMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listFilesMenuButtonActionPerformed(evt);
            }
        });
        fileMenu.add(listFilesMenuButton);

        jMenuBar1.add(fileMenu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 536, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 363, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void fileMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileMenuActionPerformed
    }//GEN-LAST:event_fileMenuActionPerformed

    private void loadMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadMenuButtonActionPerformed
        String fileName = (String) JOptionPane.showInputDialog("File To Load: ");
        if (fileName == null && (fileName.length() > 0)) {
            return;
        } else {
            this.mainPanel.setTextInTextArea(this.fileSystem.loadFile(fileName));
        }
    }//GEN-LAST:event_loadMenuButtonActionPerformed

    private void saveMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveMenuButtonActionPerformed
        String fileName = (String) JOptionPane.showInputDialog("File Name: ");
        if (fileName == null && (fileName.length() > 0)) {
            return;
        } else {
            this.fileSystem.createFile(fileName, this.mainPanel.getTextInTextArea());
        }
    }//GEN-LAST:event_saveMenuButtonActionPerformed

    private void deleteMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteMenuButtonActionPerformed
        String fileName = (String) JOptionPane.showInputDialog("File To Delete: ");
        if (fileName == null && (fileName.length() > 0)) {
            return;
        } else {
            this.fileSystem.deleteFile(fileName);
        }
    }//GEN-LAST:event_deleteMenuButtonActionPerformed

    private void deleteAllMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteAllMenuButtonActionPerformed
        int confirmSelection = JOptionPane.showConfirmDialog(this, "Delete All Files?");
        if (confirmSelection == 0) {
            this.fileSystem.deleteAllFiles();
        }
    }//GEN-LAST:event_deleteAllMenuButtonActionPerformed

    private void listFilesMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listFilesMenuButtonActionPerformed
        JOptionPane.showMessageDialog(this, this.fileSystem.listFiles());
    }//GEN-LAST:event_listFilesMenuButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem deleteAllMenuButton;
    private javax.swing.JMenuItem deleteMenuButton;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem listFilesMenuButton;
    private javax.swing.JMenuItem loadMenuButton;
    private javax.swing.JMenuItem saveMenuButton;
    // End of variables declaration//GEN-END:variables
}
